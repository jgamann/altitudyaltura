package com.jgamann.altitudyaltura.utils;

/**
 * Created by txus on 11/10/15.
 */
public class Constants {
    public static final int ITMER_LOCATION_SEARCH = 15000;
    public static final int GOOD_ACCURACY = 15;
    public static final int MEDIUM_ACCURACY = 100;
}
