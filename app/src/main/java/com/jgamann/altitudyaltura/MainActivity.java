package com.jgamann.altitudyaltura;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.jgamann.altitudyaltura.utils.Constants;
import com.jgamann.altitudyaltura.utils.Utils;

public class MainActivity extends AppCompatActivity {

    private static LocationManager mLocationManager;
    static ProgressDialog ringProgressDialog;
    private static Handler handlerUnknownActivity = new Handler();
    private static Runnable lastTimer;
    static FrameLayout framePrincipal;
    static Context context;
    private static TextView altitudeInfo;
    private static TextView degminsecLatInfo;
    private static TextView degminsecLonInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        altitudeInfo = (TextView) findViewById(R.id.altitudeInfo);
        degminsecLatInfo = (TextView) findViewById(R.id.degminsecLatInfo);
        degminsecLonInfo = (TextView) findViewById(R.id.degminsecLonInfo);
        setSupportActionBar(toolbar);
        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        framePrincipal = (FrameLayout) findViewById(R.id.frame_principal);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activateLocation(view);
            }
        });
        context = getApplicationContext();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private Runnable timedUnknownPosition = new Runnable() {
        public void run() {
            LocationTimeout();
        }
    };


    private LocationListener mGPSListener = new LocationListener() {

        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        public void onLocationChanged(Location location) {
            if(location.getAccuracy()<Constants.GOOD_ACCURACY){
                closeDialog();
                if(lastTimer!=null)handlerUnknownActivity.removeCallbacks(lastTimer);
                lastTimer=null;
                //Pintar resultado en la pantalla. Tiene una resolución buena, lo damos por zanjado, pintar y parar listener.
                printResultsDefinit(location);
                stopLocationUpdates();
            }else if(location.getAccuracy()<Constants.MEDIUM_ACCURACY){
                closeDialog();
                if(lastTimer!=null){
                    handlerUnknownActivity.removeCallbacks(lastTimer);
                    handlerUnknownActivity.postDelayed(lastTimer, Constants.ITMER_LOCATION_SEARCH);
                }
                //Pintar resultado en la pantalla provisional y volver a empezar el timer de timeout para darle más oportunidades.
                printResults(location);
            }else{//le damos otro intento porque tiene un accuracy malo
                if(lastTimer!=null){
                    handlerUnknownActivity.removeCallbacks(lastTimer);
                    handlerUnknownActivity.postDelayed(lastTimer, Constants.ITMER_LOCATION_SEARCH);
                }
            }
        }

        public void onProviderEnabled(String provider) {

        }

        public void onProviderDisabled(String provider) {

        }
    };


    public void activateLocation(View view) {
        if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            lastTimer = timedUnknownPosition;
            handlerUnknownActivity.postDelayed(timedUnknownPosition, Constants.ITMER_LOCATION_SEARCH);
            ringProgressDialog = ProgressDialog.show(MainActivity.this,
                    "Por favor espere ...", "Detectando posición y altura ...", true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for Activity#requestPermissions for more details.
                    return;
                }
            }
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mGPSListener);
        } else {
            showAlertBottom(view, R.string.gps_is_required);
            /*Snackbar.make(view, R.string.gps_is_required, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();*/

        }

    }

    public void LocationTimeout() {
        closeDialog();
        showAlert(MainActivity.this, getApplicationContext().getText(R.string.location_timeout_msg).toString());
        stopLocationUpdates();
    }


    public static void showAlert(Activity act, String body) {
        AlertDialog alertDialog = new AlertDialog.Builder(act).create();
        alertDialog.setTitle(R.string.information_title);
        alertDialog.setMessage(body);
        alertDialog.show();
    }

    public static void showAlertBottom(View view, int body) {
        Snackbar.make(view, body, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    public static void printResults(Location _loc) {
        if(_loc!=null && _loc.getAltitude()>-1){
            altitudeInfo.setText(_loc.getAltitude()+"m");
            degminsecLatInfo.setText(Utils.getDegMinSecLat(_loc));
            degminsecLonInfo.setText(Utils.getDegMinSecLon(_loc));
        }
    }

    public static void printResultsDefinit(Location _loc) {
        int fotoId = Utils.getRandom(3);
        switch (fotoId){
            case 1:
                framePrincipal.setBackgroundResource(R.drawable.atardecer);
                break;
            case 2:
                framePrincipal.setBackgroundResource(R.drawable.nieve);
                break;
            case 3:
                framePrincipal.setBackgroundResource(R.drawable.parque);
                break;
        }
        if(_loc!=null && _loc.getAltitude()>-1){
            altitudeInfo.setText(_loc.getAltitude()+"m");
            degminsecLatInfo.setText(Utils.getDegMinSecLat(_loc));
            degminsecLonInfo.setText(Utils.getDegMinSecLon(_loc));
        }
    }

    public static void closeDialog(){
        if(ringProgressDialog!=null)ringProgressDialog.dismiss();
    }

    public void stopLocationUpdates() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
        mLocationManager.removeUpdates(mGPSListener);
    }


}
